package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {
        // 1.Будем исп-ть генератор псевдослуч. чисел и считывать ввод с консоли

        // Random it is a special class to generate numbers
        // Каждый класс это по факту еще один тип (ссылочный)

        Random generator = new Random(); // инициализируем переменную generator
        int secret = generator.nextInt(4);
        //nextInt () -> [0; Integer.MAX_VALUE]
        //nextInt (4) -> [0;3] если указать в скобках у метода nextInt
        //nextInt (11)+10 -> [10;20]

        //Ввод с консоли
        //Scanner - специальный класс, который позволяет считывать ввод с консоли
        System.out.println("Введите число ");
        Scanner scanner1 = new Scanner(System.in);
        int number = scanner1.nextInt(); // просим записать эту новую переменную scanner1, которую получим из консоли в переменную number


        System.out.println(secret);
        System.out.println(number);


        //if (condition) { ...}
        //else {}

        // boolean condition = number ==secret; так не делается :)
//        if (number == secret) {
//            System.out.println("Победа! Вы угадали!");
//        } else {
//            System.out.println("Беда! Вы не угадали! :(");
//            if (number < secret )          {
//                System.out.println("Вы ввели число меньше, чем загаданное");
//            } else {
//                System.out.println( "Вы ввели число больше, чем загаданное");
//            }
//        }

        if (number == secret) {
            System.out.println("Победа! Вы угадали!");
        } else if (number < secret ) {
            System.out.println("Вы ввели число меньше, чем загаданное");
        } else {
            System.out.println( "Вы ввели число больше, чем загаданное");
        }




        }
}
