package org.levelup.lesson2;

public class Arithmetic {

    public static void main(String[] args) {
        int a = 34;
        int b = 4;
        double result1 = a/b;
        System.out.println(result1);


        int a1 = 45;
        double b1 = 4;
        double result2 = a1/b1;
        System.out.println(result2);

        //Преобразование типов
        long longVar = a; //int -> long неявное преобразование типов (расш.), если разрешено неявное, то допускается и явное, вдруг ;)
        int intVar = (int) longVar; // явное преобразование типов, когда указываем в какой тип
        //т.к. тип справа от = больше, чем тип слева от ==

        double division =  (double) (a/b); //8.0
        System.out.println(division);

        //такое явное преобразование используется чаще, и более логично
        double division1 = (double) a /b;
        System.out.println(division1);


        byte bigByte  = (byte) 128; //result is -128
        System.out.println(bigByte);


    }
}
