package org.levelup.lesson2;

import org.levelup.lesson4.homework.Reader;

import java.util.Random;

public class Arrays {

    public static void main(String[] args) {
        double price1 = 444.43;
        double price2 = 985.89;
        double price3 = 11288.99;
        double price4 = 777.9;

        double averagePrice = (price1 + price2 + price3 + price4) / 4;
        System.out.println("Average price is " + averagePrice);

        // Array [] масссив типа дабл который назвается цены = новая переменная типа дабл с 4 эл-ми

        double [] prices = new double[4] ;
        prices [0] = 444.43;
        prices [1] = 985.89;
        prices [2] = 11288.99;
        prices [3] = 777.9;

        System.out.println("prices [0]: " + prices [0]);

        for (int i=0; i< prices.length; i++) {
            System.out.println(i + "-" + prices [i]);
        }

        //
        int [] array = new int [10];
        Random generator = new Random();

        for (int i = 0; i < array.length; i++) {
            array [i] = generator.nextInt(100);
        }


        int sum = 0;
        for (int i = 0; i < array.length; i =i+3) {
            sum = sum+ array[i];
        }
        double avg = sum / (double) array.length;
        System.out.println("avg is " + avg);



    }

}
