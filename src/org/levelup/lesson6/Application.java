package org.levelup.lesson6;

import java.util.SortedMap;

public class Application {

    public static void main(String[] args) {
        University existUniversity  = new University("ИТМО", 1804);
        University notExistUniversity = new University("Привет", 1809);

        UniversityInMemoryStorage inMemoryStorage = new UniversityInMemoryStorage();

        boolean exist = inMemoryStorage.exist(existUniversity);
        System.out.println("Университет " + existUniversity.getName() +" существует: " + exist);

        exist = inMemoryStorage.exist(notExistUniversity);
        System.out.println("Университет " + notExistUniversity.getName()+ " существует: " +exist);


        InMemoryStorage stringStorage = new InMemoryStorage();
        stringStorage.addToArray("Samsung");
        stringStorage.addToArray("APple");
        stringStorage.addToArray("Xiaomi");

        System.out.println("Samsung exist: " +stringStorage.exist("Samsung"));
        System.out.println();

        InMemoryStorage universitiesStorage = new InMemoryStorage();
        universitiesStorage.addToArray(new University("ИТМО", 1804));
        universitiesStorage.addToArray(new University("Лэти", 1450));
        universitiesStorage.addToArray(new University("Финэк", 1450));
        universitiesStorage.addToArray(new University("Политех", 1800));

        System.out.println("ИТМО существует " + universitiesStorage.exist(existUniversity));
        System.out.println("Финэк существует?" +universitiesStorage.exist(notExistUniversity) );



    }
}
