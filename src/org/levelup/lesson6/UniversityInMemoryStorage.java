package org.levelup.lesson6;

public class UniversityInMemoryStorage {

    private University[] universities;

    public UniversityInMemoryStorage() {
        this.universities = new University[10];
        this.universities[0] = new University("СПбГУ", 1784);
        this.universities[1] = new University("ИТМО", 1804);
        this.universities[2] = new University("Политех", 1800);
    }

    public boolean exist(University university) {
        for (int i = 0; i < universities.length; i++) {
            if (universities[i] != null) {
                if (university.hashCode() == universities[i].hashCode() && university.equals(universities[i])) {
                    return true;
                }
            }
        }
        //если цикл завершится и мы сюда пришли, значит что мы не нашли университет, который есть в памяти
        return false;
    }
}

