package org.levelup.lesson6.homework;

import org.levelup.lesson6.homework.Numbers;

public class Application {

    public static void main(String[] args) {
        System.out.println("------------------Task1----------------------");
        //Создайте класс любой сущности на выбор. Определите у нее несколько полей, конструктор с параметрами и переопределите методы equals/hashCode.
        // Далее в методе main создайте массив из объектов этого класса. И попробуйте с помощью цикла и методов hashCode equals найти объект в массиве.
        Books availableBook = new Books("Преступление и наказание", 1866);
        Books notAvailableBook = new Books("Мертвые души", 1852);

        Library library = new Library();

        boolean existInLibrary = library.existInLibrary(availableBook);
        System.out.println("Есть ли книга " + availableBook.getName() + " в библиотеке? : " + existInLibrary);

        existInLibrary = library.existInLibrary(notAvailableBook);
        System.out.println("Есть ли книга " + notAvailableBook.getName() + " в библиотеке? : " + existInLibrary);

        System.out.println("------------------Task2----------------------");
        //Реализовать поиск числа в массиве (проходим по массиву от начала до конца, и если такой элемент в массиве есть,  возвращаем его индекс).
        //Сигнатура метода: int search(int[] , int value)
        //
        //Реализовать поиск числа в массиве с помощью бинарного поиска (binarySearch). Если значение в массиве присутствует, то  возвращаем его индекс. Напомню - массив для бинарного поиска должен быть отсортирован! (сортировать не нужно, просто когда будете создавать массив, учтите это и задайте значение в порядке возрастания).
        //Сигнатура метода: int binarySearch(int[] , int value)


        //поиск элемента, но не в методе, а обычным циклом
            int[] numbers = new int[]{45, 899, 75658, 89, 90};
            int numberSearched = 2;

            int i;
            for (i= 0; i < numbers.length; i++) {
                if (numbers [i] == numberSearched)
                { System.out.println("Искомое число: " + numberSearched + " есть в массиве и имеет индекс - " +i);}
                else if (numbers [i] != numberSearched) {
                    System.out.println("Искомое число: " + numberSearched + " отсутствует в массиве");
                }
            }


            // поиск как в задании

        int avNumber = 45;
        int notAvNumber = 99;

        Numbers numbers1 = new Numbers();

        int exist = numbers1.search (numbers, avNumber);
        System.out.println(avNumber+ " искомое число содержится в масссиве");


        exist = numbers1.search (numbers, notAvNumber);
        System.out.println(notAvNumber + " искомое число не содержится в масссиве");



        System.out.println("-----------------------------Task3--------------------");

//        Product [] arr = new Product {
//            new Product("A", "B", 102);
//            new Product("AB", "BC", 189);
//            new Product("ABC", "CD", 192);
//        }

        //toString можно переопределить автоматически














    }
}
