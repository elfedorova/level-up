package org.levelup.lesson6.homework;

public class Product {

   private String brand;
   private String model;
   private double weight;

    public Product (String brand, String model, double weight) {
        this.brand = brand;
        this.model = model;
        this.weight = weight;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getWeight() {
        return weight;
    }
}
