package org.levelup.lesson6.homework;

public class Numbers {


        public int numbers[] = new int[]{45, 899, 75658, 89, 90};

        public int numberSearched = 89;

        public int search(int[] numbers, int numberSearched) {
        for (int index = 0; index < numbers.length; index++) {
            if (index == numberSearched)
                return index;
        }
        return -1;
        }

        public void print(int numberSearched, int index) {
        if (index == -1) {
            System.out.println(numberSearched + "Искомое число отсутствует в массиве");
        } else {
            System.out.println(numberSearched + "Искомое число найдено и имеет индекс:  " + index);
        }
        }


        public int numbers2 [] = new int[] {35, 56, 123, 678};
        public int numberSearched2 = 56;


        public int binarySearch  (int [] numbers2, int numberSearched2) {
        int left = -1; //т.к. начинается индексация с 0
        int right = numbers2.length;

        while (left < right) {
            int medium = (left + right) / 2;
            if (medium == numberSearched2) {
                return medium;
            } else if (numbers2[medium] < numberSearched2)
                left = medium + 1;
            else if (numbers2[medium] > numberSearched2)
                right = medium - 1;
        }
        return -1;
        }


        public void print2 (int numberSearched2, int medium) {
        if (medium == -1) {
            System.out.println(numberSearched2+ "Искомое число отсутствует в массиве");
        } else {
            System.out.println(numberSearched2 + "Искомое число найдено и имеет индекс:  " + medium);
        }
        }


}