package org.levelup.lesson6.homework;

import java.util.Objects;

public class Books {

    String name;
    int yearOfPublication;

    public Books (String name, int yearOfPublication) {
        this.name = name;
        this.yearOfPublication = yearOfPublication;
    }

    //getter чтобы обращаться из другого класса
    public String getName() {
        return name;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    @Override
    public boolean equals (Object object) {
        if (this == object) return true;
        if (object ==null || getClass() !=object.getClass()) return false;
        Books other = (Books) object;
        return yearOfPublication == other.yearOfPublication && Objects.equals(name, other.name);

    }

    @Override
    public int hashCode() {
        return Objects.hash(name, yearOfPublication);
    }


}
