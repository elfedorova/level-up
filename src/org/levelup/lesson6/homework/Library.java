package org.levelup.lesson6.homework;

public class Library {

    private Books [] books;

    public Library (){
        this.books = new Books[5];
        this.books [0] = new Books("Война и мир", 1865);
        this.books [1] = new Books("Преступление и наказание",1866);
        this.books [2] = new Books("Отцы и дети",1862);
    }

    public boolean existInLibrary (Books book) {
        for (int i = 0; i < books.length; i++) {
            if (books [i] != null) {
                if (book.hashCode() == books[i].hashCode () && book.equals(books[i])) {
                    return true;
                }
            }
        }
        return false;
    }

}
