package org.levelup.lesson6;

import java.util.Objects;

public class University {

    private String name;
    private int foundationYear;

    public University (String name, int foundationYear) {
        this.name = name;
        this.foundationYear = foundationYear;
    }

    public String getName() {
        return name;
    }

    public int getFoundationYear() {
        return foundationYear;
    }

    @Override
    public boolean equals (Object obj) {
        //1. Проверка ссылок
        if (this == obj) return true;
        //2. Проверка типа аргумента обж и проверка на налл
        if (obj == null || getClass() != obj.getClass()) return false;
        //3. Приводим к типу университи
        University other = (University) obj;
        //4. Сраниваем поля двух объектов
        return foundationYear == other.foundationYear &&
                Objects.equals(name, other.name);

    }
        @Override
        public int hashCode() {
//            int result = 17;
//            result = 31 * result + name.hashCode();
//            result = 31 * result + foundationYear();
//            return result;
        return Objects.hash(name, foundationYear);
        }
    }

