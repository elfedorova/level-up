package org.levelup.lesson3.homework;

public class ArraySorting {

    static void bubbleSort(int[] arrayBeforeSorting) {
        int count = 0;
        for (int i = 0; i < arrayBeforeSorting.length - 1; i++)
            if (arrayBeforeSorting[i] > arrayBeforeSorting[i + 1]) {
                int x = arrayBeforeSorting[i];
                arrayBeforeSorting[i] = arrayBeforeSorting[i + 1];
                arrayBeforeSorting[i + 1] = x;
                count++;
            }
        if (count > 0) {
            bubbleSort(arrayBeforeSorting);
        }
    }
}