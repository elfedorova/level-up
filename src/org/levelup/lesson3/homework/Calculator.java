package org.levelup.lesson3.homework;

public class Calculator {

    int a;
    int b;
    double c;
    double d;
    long e;
    long f;

   public Calculator (int a, int b) { //по умолчанию в джава констурктор создается с модификатором паблик, но лучше пропишу
        this.a = a;
        this.b = b;
    }

    public Calculator (double c, double d) {
        this.c = c;
        this.d = d;
    }

    public Calculator (long e, long f) {
        this.e = e;
        this.f = f;
    }

    //Methods for sum
    int sum (int a, int b) {
        int sumResult = a+b;
        return sumResult;
    }

    //overloading
    double sum (double c, double d) {
        double sumResult = c+d;
        return sumResult;
    }

    long sum (long e, long f) {
        long sumResult = e+f;
        return sumResult;
    }

    //Methods for substruction
    int substruction (int a, int b) {
        int substrResult1 = a - b ;
        return substrResult1;
//        int substrResult2 = b - a;
//        return substrResult2;
    }

    double substruction (double c, double d) {
        double substrResult = c - d;
        return substrResult;
//        double substrResult2 = d - c;
//        return substrResult2;
    }

    long substruction (long e, long f) {
        long substrResult = e - f;
        return substrResult;
//        long substrResult2 = f - e;
//        return substrResult2;
    }

    //Methods for multiplication
    int multiplication (int a, int b) {
        int multResult = a * b ;
        return multResult;
    }

    double multiplication (double c, double d) {
        double multResult = c * d ;
        return multResult;
    }

    long multiplication (long e, long f) {
        long multResult = e * f ;
        return multResult;
    }

    //Methods for division
    int division (int a, int b) {
        int divResult1 = a / b ;
        return divResult1;
//        int divResult2 = b/ a;
//        return divResult2;
    }

    double division (double c, double d) {
        double divResult1 = c / d ;
        return divResult1;
//        double divResult2 = d/ c;
//        return divResult2;
    }

    long division (long e, long f) {
        long divResult1 = e / f;
        return divResult1;
//        long divResult2 = f / e;
//        return divResult2;
    }



}
