package org.levelup.lesson3.homework;
import java.util.Arrays;

public class ArrayUtil {



    // здесь находим минимум
     public static int min(int[] array){
        int minValue = array[0];
        for(int i=0;i<array.length;i++){
            if(array[i] < minValue){
                minValue = array[i];
            }
        }
        return minValue;
    }

    //здесь находим максимум
    public static int max(int[] array){
        int maxValue = array[0];
        for(int i=0;i < array.length;i++){
            if(array[i] > maxValue){
            maxValue = array[i];
        }
        }
        return maxValue;
    }

}