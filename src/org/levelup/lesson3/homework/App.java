package org.levelup.lesson3.homework;

import java.util.Arrays;

import static org.levelup.lesson3.homework.ArraySorting.bubbleSort;

public class App extends ArrayUtil{

    public static void main(String[] args) {

        System.out.println("-------------------------------Task1---------------------------");

        Calculator calc1 = new Calculator(7, 90);
        Calculator calc2 = new Calculator(54.5, 780.5);
        Calculator calc3 = new Calculator(4000, 10000);

        int sum1 = calc1.sum(7, 90);
        System.out.println(sum1);

        double sum2 = calc2.sum(54.5, 780.5);
        System.out.println(sum2);

        long sum3 = calc3.sum(4000, 10000);
        System.out.println(sum3);

        int division1 = calc1.division(7, 90);
        System.out.println(division1);

        double substruction1 = calc2.substruction(54.5, 780.5);
        System.out.println(substruction1);

        long substruction2 = calc2.substruction(4000, 10000);
        System.out.println(substruction2);

        int multiplication1 = calc1.multiplication(7, 90);
        System.out.println(multiplication1);


        System.out.println("-------------------------------Task2---------------------------");
        //Написать класс ArrayUtil. У него два метода - min(int[] array) и max(int[] array). Он должен возвращать вам минимум и максимум из переданного массива соотвественно.
        // Т.е. в метод min вы передаете массив, а в ответ вам возвращается число, которое является минимум из переданного массива.


            int array2[] = new int[]{4,889,99,60,89};

            int min = min(array2);
            System.out.println("Минимальное число "+min);

            int max = max(array2);
            System.out.println("Максимальное число: " +max);



        System.out.println("-------------------------------Task3---------------------------");
        //Написать класс ArraySorting, в котором будет метод bubbleSort. Он на вход принимает массив целых чисел (int), а на выход отдает новый!
        // массив с отсортированными по возрастанию значениями.
        // Вам нужно выполнить сортировку массива с помощью сортировки пузырьком.

        int array3[] = {4,889,99,60,89};
        bubbleSort(array3);
        System.out.println(Arrays.toString(array3));




    }
}
