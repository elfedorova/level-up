package org.levelup.lesson3;
//класс, который будет содержать метод main, в нем будем писать алгоритм


public class Application {

    public static void main(String[] args) {

//        int x1 = 40;
//        int y1 = 45;
//
//        int x2 = 43;
//        int y2 = 52;
//
//        int [] xCoordinates;
//        int [] yCoordinates;

        //p1,p2 это переменная ссылочного типа - это объект
        //объект = экземпляр, ссылка, object, instance, reference

        Point p1 = new Point(); //сделали конструктор
        System.out.println("p1 before setting values (" +p1.x + ", " +p1.y + ")");
        p1.x =4;//найди поле p1 в памяти, найди там ячейку x и присвой ей значение =4;
        p1.y =56;
        p1.name = "point A";
        System.out.println("p1 - " + p1.name + " (" + p1.x + ", " +p1.y +")");
        /*когда создается такая переменная в памяти записывается некоторый указатель, hash code?
        нет, это ссылка хах ;)
         */

        p1.changePoint(12, 78);
        System.out.println("p1 - " + p1.name + " (" + p1.x + ", " +p1.y +")");
        p1.changePoint(67,90);
        System.out.println("p1 - " + p1.name + " (" + p1.x + ", " +p1.y +")");

        int p1Quadrant = p1.quadrant(); //вызываем метод и записываем результат в переменную
        System.out.println("P1 Quadrant is " + p1Quadrant);

        p1.changePoint("point B", 78, 89);

        p1.printPoint();




        Point p2; //null объект не был создан, ему не была выделена память
        Point p3 = new Point("point C", 76,98);
        Point p4 = new Point (6,70);





    }
}
