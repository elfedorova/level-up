package org.levelup.lesson5;
/*Access modifiers
    1. private = вызов метода( или поля) возможен только внутри класса
    2. default-package (private package) = private + доступ внутри пакета
    3. protected = default package + доступ из класса-наследника, даже если они в других пакетах)
    4. public = доступ отовсюду
     */

import java.util.Objects;

public class Product {

    private String name;
    private String vendorCode;
    private double price;

    public Product(String name, String vendorCode, double price) {
        this.name = name;
        this.vendorCode = vendorCode;
        this.price = price;
    }


    //setter
    //public void set <Field name> (<field type> <field name>) {
    // this.field name = field name;}
    public void setName (String name) {
        this.name = name;
    }

    //getter - получить значение поля за пределами класса
    //public <field name > get (<field name> () {
    //return <field name>; }
    public String getName () {
        return name;
    }

    @Override
    public boolean equals (Object object) {
        //this
        //object

        if (this == object) return true;

        //1 способ (чего? словить каст эксепшн?) подходит для классов , которые не родители и не имеют дочерних классов
        // instanceof
        // <переменная ссылочного типа> instanceof <имя класса> -> можно ли переменную привести к типу?
        // null instanceof <любой класс> -> false
        // if (!(object instanceof Product)) return false;

        // 2 способ (подходит для всх случаев)
        if (object == null || this.getClass() != object.getClass()) return false;

        Product other = (Product) object;
        // < 0 -> d1 < d2
        // = 0 -> d1 = d2
        // > 0 -> d1 > d2
        // Double.compare(d1, d2) == 0 -> true/false

        // this.vendorCode = null;
        // other.vendorCode = null;
        // (vendorCode == other.vendorCode || (vendorCode != null && vendorCode.equals(other.vendorCode)))
        return Objects.equals(vendorCode, other.vendorCode) && Double.compare(price, other.price) == 0;

//        if (vendorCode.equals(other.vendorCode) && Double.compare(price, other.price) == 0) {
//            return true;}

//        return false;

//         return vendorCode.equals(other.vendorCode) && Double.compare(price, other.price) == 0;


    }

}
