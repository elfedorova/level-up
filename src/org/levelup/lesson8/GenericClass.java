package org.levelup.lesson8;

//generic некий параметр, можно сделать параметризированные классы <>
// <T> type alias
public class GenericClass <T> {

    private T field;

    public T getField() {
        return field;
    }

    public void setField(T field) {
        this.field = field;
    }

}
