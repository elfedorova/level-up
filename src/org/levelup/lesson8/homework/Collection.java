package org.levelup.lesson8.homework;

import java.util.*;
import java.util.Optional;

/* Задание2
Сгенерируйте коллекцию целых чисел (любой тип коллекции, но для простоты можете использовать ArrayList), используя класс Random().
Используя методы классов-коллекций:

*ctrl I создать оверрайд
 */
public class Collection {

    public static void main(String[] args) {

        List <Integer> numbers = new ArrayList<>();
        Random generator8 = new Random();

        HashSet <Integer> numbers3 = new HashSet<>();
        numbers3.add(34);
        numbers3.add(7);
        numbers3.add(67);
        numbers3.add(860);
        numbers3.add(400);
        numbers3.add(34);

        for (int i = 0; i < 10; i++) {
            int number = generator8.nextInt((1)+999);
            numbers.add(number);
        }
        System.out.println("Размер коллекции: " + numbers.size());

        //для проверки метода добавления только положителных чисел, нахождения по индексу, отр
//        numbers.add(78);
//        numbers.add(-98);
//        numbers.add(-4568);
//        numbers.add(78);
//        numbers.add(908);


        //цикл for each нужен нам для того, чтобы итерироваться по коллекции
        for (Integer number : numbers) {
            System.out.println(number);
        }

        //1.Создайте новую коллекцию, переписав в нее часть элементов из первой коллекции (к примеру, первые 10 элементов)"

        //Collections.copy(List <Integer> numbers2, List<Integer> numbers);
        //System.out.println(numbers2);
        List <Integer> numbers2 = new ArrayList<>();
        numbers2 = numbers.subList(0,5); // первые пять эл-тов
        System.out.println("Первые 5 элементов первоначал. коллекции - "+ numbers2);

        //2.Найдите все уникальные числа (те, которые встречаются один раз) и сохраните их в отдельную коллекцию

        System.out.println( "Коллекция с только уникальными элементами" +numbers3);



        //3.Отсортируйте коллекцию (по возрастанию/убыванию
        Collections.sort(numbers);
        System.out.println("Отсортированная коллекция по возрастанию: " + numbers);

        Collections.reverse (numbers);
        System.out.println("По убыванию: " + numbers);


        //4. Найдите максимум, минимум и сумму чисел коллекции
        Optional <Integer> min = numbers.stream().min(Integer ::compare);
        System.out.println("Минимальное число в коллекции: " + min.get());

        Optional <Integer> max = numbers.stream().max(Integer ::compare);
        System.out.println("Максимальное число в коллекции: " + max.get());

//        int count = 0;
//        for (int i = 0; i < numbers.size(); i++) {
//            count = count +i;
//        }
//        System.out.println(count);

        int sum = 0;
        for (Integer number : numbers) {
            sum = sum + number;
        }
        System.out.println("Сумма чисел в коллекции = " + sum);


        //5. Создайте  коллекцию, содержащую все положительные числа первой коллекции
        List <Integer> numbers5 = new ArrayList<>();
        for (Integer number : numbers ) {
            if (number > 0) {
                numbers5.add(number);
            }
        }
        System.out.println("Все положительные числа из первоначал. коллекции" + numbers5);

        //6. Удалите из первой коллекции все нечетные числа

//        for (Integer number : numbers) {
//            if (number % 2 != 0) {
//                numbers.remove(number);
//            }
//        }  не работает, т.к. не можем менять коллекцию и вылетает concurrentModificationException
//        System.out.println(numbers);


        numbers.removeIf( n -> (n % 2 != 0)); // не поняла такую  запись -> если введенная переменная удовлетворяет условию в () ?
        System.out.println("Коллекция только с четными числами " +numbers);

        //7. Найдите индекс указанного числа в коллекции
        System.out.println(numbers.indexOf(78));



























    }
}
