package org.levelup.lesson8.homework;

import org.levelup.lesson4.homework.Book;

import java.security.GeneralSecurityException;

public class GenericsApplication {

    public static void main(String[] args) {

        Generics <Integer> generic1 = new Generics<>();
        generic1.setField(89);
        System.out.println(generic1.getField());

        Generics <Double> generic2 = new Generics<> ();
        generic2.setField(8988.90);
        System.out.println(generic2.getField());

        Generics <Book> generic3 = new Generics<>();
        generic3.setField(new Book("Gesse", "Ansicht des Clowns"));
        System.out.println(generic3.getField());

    }
}
