package org.levelup.lesson8.homework;

public class Generics <T> {

    private T field;

    public T getField() {
        return field;
    }

    public void setField(T field) {
        this.field = field;
    }

}
