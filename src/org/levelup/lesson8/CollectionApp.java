package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.List;

public class CollectionApp {

    public static void main(String[] args) {


        List<String> universityNames = new ArrayList<>();

        universityNames.add("СПбГУ");
        universityNames.add("ИнжЭкон");
        universityNames.add("СПбГПУ");
        universityNames.add("ПГУПС");

        // for (<type> <var_name> : <collection>)
        for (String name : universityNames) {
            System.out.println(name);
        }

        System.out.println("Есть ли ВУЗ ЛЭТИ в коллекции? - " + universityNames.contains("ЛЭТИ"));
        System.out.println("Под индексом 2 ВУЗ: - " + universityNames.get(2));
    }


}
