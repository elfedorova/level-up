package org.levelup.lesson8;

import org.levelup.lesson5.Product;
import org.levelup.lesson8.homework.Generics;

public class GenericClassApplication {

    public static void main(String[] args) {
        GenericClass <String> stringGC = new GenericClass<>();
        stringGC.setField("Hiiiii");

        GenericClass <Product> productGC = new GenericClass<>();
        productGC.setField(new Product("a", "b", 787.80));

        GenericClass rawTypeGC = new GenericClass();
        rawTypeGC.setField(new Object());

        GenericClass<String> stringGenericClass = new GenericClass<>();
        stringGenericClass.setField("Hello again");
        String field = stringGenericClass.getField();


    }
}
