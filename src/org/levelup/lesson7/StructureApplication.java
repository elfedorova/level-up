package org.levelup.lesson7;

import org.levelup.lesson7.structures.AbstractList;
import org.levelup.lesson7.structures.DynamicArray;
import org.levelup.lesson7.structures.OneWayList;
import org.levelup.lesson7.structures.Structure;

public class StructureApplication {

    public static void main(String[] args) {


        AbstractList <Integer> list = new OneWayList();
        addAndPrintCount (list);

        Structure <Integer> s = new DynamicArray();
        s.add(45);
        s.add(23);
        s.add(12);


    }

    static void addAndPrintCount (AbstractList list) {
        list.add(34);
        list.add(56);
        list.add(789);
        list.add(7);
        list.add(344);
        list.add(1);
        list.add(8);

        int [] array = {3,4};
        list.addArray(array);
        System.out.println("List size " + list.getSize());

    }


}
