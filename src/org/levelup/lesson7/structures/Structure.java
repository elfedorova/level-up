package org.levelup.lesson7.structures;

//интерфейс - это чистый абстрактный класс
//методы реализованные в нем, должны быть обязательно переопредлены в классе который имплементирует интерфейс

public interface Structure <T> {

    void add(int value);

    void remove (int value);

    void removeByIndex (int index);

    int getSize ();

    boolean isEmpty ();

}
