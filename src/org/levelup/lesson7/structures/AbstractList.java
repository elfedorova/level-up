package org.levelup.lesson7.structures;

/*Абстрактный класс, используется для создания наследников, иначе бесполезны.
Выносят туда обычно функционал общий для классов-наследников.
Обычный Java класс, но:
1. Нельзя создать объект абстрактного класса
2. Абстрактный класс может иметь абстрактные методы (только такой класс может содержать (но не обязан) абстрактные методы)
 */

public abstract class AbstractList <T> implements Structure {

    protected int size;

    public abstract void add (int value);

    //addArray (int [] array);
    public void addArray (int [] array) {
        for (int i = 0; i <array.length; i++) {
            add(array[i]);
        }
    }

    public int getSize () {
        return size;
    }

    @Override
    public boolean isEmpty () {
        return size ==0;
    }


}
