package org.levelup.lesson7.structures;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//Однонаправленный связный список
public class OneWayList <T> extends AbstractList {

    //Только размер списка
//    private int size;
    private ListElement head;


    //у последнего эл-та в списках next всегда равен 0

    //add(3):
    // el1(34) -> el2(54) ->el3(23)
    //el4(57)


    @Override
    public String toString() {
        return "OneWayList{" +
                "head=" + head +
                '}';
    }

    @Override
    public void add(int value) {
        ListElement el = new ListElement(value);
        if (head == null) //проверяем не пустой ли он
        {
            head = el;
        } else {
            ListElement cursor = head;
            while (cursor.getNext() != null) {
                cursor = cursor.getNext();
            }
            //cursor указывает на посл.элемент списка
            cursor.setNext(el);
        }
        size++;

    }

    //Удаление элемента из списка по его значению
    @Override
    public void remove(int value) {
        ListElement element = new ListElement(value);
        if (element.equals(value)){
            element = null;
            System.out.println("Элемент со значением: " + value + " только что был удален из списка");
        } else {
            System.out.println("Элемента со значением: " + value + " нет в списке");
        }
    }


    //Удаление элемента из списка по индексу
    @Override
    public void removeByIndex(int index) {
            ListElement element = new ListElement(index);
        if (index <= size) {
            element = null;
            System.out.println("Элемент под индексом [" + index + "] только что был удален из списка");
        } else {
            System.out.println("Элемента под индексом [" + index + "] нет в списке");
        }
    }
}
