package org.levelup.lesson7.structures;

 /*Структура данных как список на основе массива
    Список - набор однотипных элементов
     - присутствуют дубликаты
     - у списков есть индексы (т.е. можно получить элемент списка по индексу)
     - вставка
         -в начало, в середину, в конец
            [1, 44, 85, 23]
            add(int index, int value) -> add (1, 5);
            [1, 44, 85, 23] -> [1, 5, 44, 85, 23]
            add (int value) -> add(6) это вставка в конец списка
            [1, 44, 85, 23] -> [1, 5, 44, 85, 23, 6]
            addFirst (int value) -> addFirst (50)
            [1, 44, 85, 23] -> [50, 1, 5, 44, 85, 23]

       -удаление
            -из начала, из середины, из конца

            //new Object [59] -> DynamicArray -> add()


 */

import java.util.Arrays;

public class DynamicArray <T> extends AbstractList {

    private int[] elements;
    //переменная size ыполняет две функции: отображает размер списка (кол-во эл-тов в списке)
    //и индексы для вставки в конец массова elements

    //elements: [0, 0, 0], size = 0;
    //add(78): [78, 0, 0], size = 1;
    //add(98): [78, 98, 0], size = 2;
    //add(4): [78, 98, 4], size = 3;
    //add(12): [78, 98, 4, 0, 0, 0] -> [78, 98, 4, 12, 0, 0], size = 4;

//    private int size;

    public DynamicArray() {
        this.elements = new int[3];
        this.size = 0; //можно не прописывать, т.к. примитивы инициализируются дефолтными значениями = 0

    }

    //добавляет эл-т в конец списка по умолчанию


    @Override
    public String toString() {
        return "DynamicArray{" +
                "elements=" + Arrays.toString(elements) +
                '}';
    }

    @Override
    public void add(int value) {
        if (elements.length == size) {
            ensureCapacity();
        }
        elements[size] = value;
        size++; //-> size = size +1;
    }

    //добавляет знач-е в начало списка
    public void addFirst (int value) {
        if (elements.length == size) {
            ensureCapacity();
         }
        System.arraycopy(elements, 0, elements, 1, size -1);
        elements [0] = value;
        size ++;
    }

    private void ensureCapacity () {
        //1. Сохранение старого массива(текущего)
        int  [] temp = elements;
        // 2.Создание нового массива
        elements = new int [temp.length *2];
        // 3. Копирование старого массива в новый
        System.arraycopy(temp, 0, elements, 0, temp.length);

    }

    //Удаление элемента из массива по его значению
    @Override
    public void remove(int value) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == value) {
                elements[i] = 0;
                System.out.println("Элемент со значением - " + value + " только что был удален из массива");
            }
        }
        System.out.println("Элемента c таким значением - " + value + " нет в массиве");
        }



    //Удаление элемента из массива по индексу
    @Override
    public void removeByIndex(int index) {
        if (index <= size) {
            elements [index] = 0;
            System.out.println("Элемент под индексом [" + index + "] только что был удален из массива");
        } else {
            System.out.println("Элемента под индексом [" + index + "] нет в массиве");
    }
}




}


