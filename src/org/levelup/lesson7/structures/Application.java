package org.levelup.lesson7.structures;

import org.levelup.lesson7.structures.DynamicArray;

public class Application {

    public static void main(String[] args) {

        int [] array = new int [10];
        array [0] = 34;
        array [1] = 36;


        DynamicArray dynamicArray = new DynamicArray();
        dynamicArray.add(34);
        dynamicArray.add(56);
        dynamicArray.add(789);
        dynamicArray.add(7);
        dynamicArray.add(344);
        dynamicArray.add(1);
        dynamicArray.add(8);

        OneWayList oneWayList = new OneWayList();
        oneWayList.add(34);
        oneWayList.add(54);
        oneWayList.add(23);


        System.out.println( "Массив до удаления: "+dynamicArray.toString());
        dynamicArray.removeByIndex(3);
        System.out.println("Массив после удаления: "+ dynamicArray.toString());


        System.out.println( "Массив до удаления: "+dynamicArray.toString());
        dynamicArray.remove(34);
        System.out.println("Массив после удаления: "+ dynamicArray.toString());

        System.out.println("------one way list-----");
        System.out.println("Список до удаления: " + oneWayList.toString());
        oneWayList.removeByIndex(4);

        oneWayList.remove(54);
        System.out.println("Список после удаления: " + oneWayList.toString());












    }

}
