package org.levelup.lesson1;

public class Variables {

        //psvm + tab /enter -> public static void main
        //sout - > System.out.println

        public static void main(String[] args) {

        int firstInteger = 123;
        int secondInteger = 456;

        int sum = firstInteger + secondInteger;
        System.out.println(sum);

        char firstChar = 'H';
        char secondChar = 72;
        System.out.println(firstChar);
        System.out.println(secondChar);

        float firstFloat = 435.566f;
        float secondFloat = 556.66f;

        System.out.println(firstFloat + secondFloat);

        //in this case result will be a String without summation
        System.out.println("Sum" + firstFloat +secondFloat);


    }

}
