package org.levelup.lesson1;

public class Lesson1 {

    public static void main(String[] args) {

        //Task1
        System.out.println ("------------------------------Task2--------------------------");
        int a = 47;
        int b = 290;
        int result1 = a+b;
        System.out.println(result1);

        byte c = 8;
        short d = 678;
        int result2 = c +d ;
        System.out.println(result2);

        int e = 2344;
        long f = 899000L;
        long result3 = f - e;
        System.out.println(result3);

        int g = 3456;
        double h = 43.5;
        double result4 = g / h;
        System.out.println(result4);

        int i = 777;
        double j = 5.76;
        double result5 = i % j;
        System.out.println(result5);

        System.out.println ("------------------------------Task3--------------------------");

        //Решение уравнения ax + b = 0, где а и б заданы

        int a1 = -5;
        int b1 = 10;
        int x = -b1 /a1;

        System.out.println( x);
    }


}
