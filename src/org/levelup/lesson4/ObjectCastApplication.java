package org.levelup.lesson4;

public class ObjectCastApplication {

    public static void main(String[] args) {

        Rectangle r = new Rectangle();
        //r.square();

        Shape s = r; //неявное расширяющее преобразование
       // s.square (); не можем уже вызвать т.к. этого метода нет в род. классе shape

        Object o = r; //неявное расширяющее преобразование
        //o.perimeter ();

        Shape so = (Shape) o; //явное сужающее преобразование
        Rectangle rs = (Rectangle) so; //явное сужающее преобразование

//        Shape sh = (new int [0] {2,3, 5, 7,8});
//        System.out.println(s.perimeter());
//        System.out.println(sh.perimeter());
//        Rectangle rsh = (Rectangle) sh; produces ClassCastException





    }
}
