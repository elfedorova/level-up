package org.levelup.lesson4.homework;

import java.util.Arrays;

public class Reader {

    String nameOfUser;
    int readersTicketNumber;
    String faculty;
    String dateOfBirthday;
    int phoneNumber;



    public Reader (String nameOfUser, int readersTicketNumber, String faculty, String dateOfBirthday, int phoneNumber) {
        this.nameOfUser = nameOfUser;
        this.readersTicketNumber = readersTicketNumber;
        this.faculty = faculty;
        this.dateOfBirthday = dateOfBirthday;
        this.phoneNumber = phoneNumber;
    }

    static void showReaders (Reader [] readers) {
        System.out.print("Список читателей запретной секции: ");
        System.out.print(Arrays.deepToString(readers));
    }

    // надо как то переопредлить метод ту.стринг чтобы он возвращал мне не хэш код

    @Override
    public String toString () {
        return this.nameOfUser;
    }


    public void takeBook (int amountOfBooks) {
        System.out.println(this.nameOfUser + " взял(-а) на прокат " + amountOfBooks + " книг(у)");
    }

    public void takeBook (String [] books) {
        System.out.println(this.nameOfUser +  " взял(-а) на прокат следующие книги: " + Arrays.deepToString(books));
    }

//    public void takeBook (Book [] books2) {
//        System.out.println(this.nameOfUser + " взял(-а) на прокат книги: " + Arrays.toString(books2)); //books2.toString());
//    }

    public void takeBook (Book [] books2) {
        System.out.print(this.nameOfUser + " взял(-а) на прокат книги: ");
        for (Book book: books2) {
            System.out.print(book.nameOfBook);  // так возвращается не хэш код, а значение
        }
        System.out.println();
    }




    public void returnBook (int amountOfBooks) {
        System.out.println(this.nameOfUser + " вернул(-а) " + amountOfBooks + " книг(у)");
    }

    public void returnBook (String [] returnedBooks) {
        System.out.println(this.nameOfUser + " вернул(-а) " + Arrays.deepToString(returnedBooks));
    }

    public void returnBook (Book [] returnedBooks2) {
        System.out.print(this.nameOfUser + " вернул(-а): " );
        for (Book book: returnedBooks2) {
            System.out.print(book.nameOfBook + ", ");
        }
        System.out.println();
    }





}
