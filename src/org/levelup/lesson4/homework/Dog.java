package org.levelup.lesson4.homework;

 public class Dog extends Animal {

  boolean huntingDog;

 public Dog () {

 }

 public Dog (String name, String food, String location) {
  super(name, food, location);
 }

 public Dog (String name, String food, String location, boolean huntingDog) {
  super(name, food, location);  //Вызываю в конструкторе с 3 мя пар-ми конструктор с 2мя
  this.huntingDog = huntingDog;
 }

 @Override
 public void makeNoise() {
  System.out.println("Гав-гав");
 }

 @Override
 public void eat () {
  System.out.println("Собака ест");
 }

  @Override
  public void sleep () {
   System.out.println("Собака спит zzz");
  }




}
