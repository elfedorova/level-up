package org.levelup.lesson4.homework;

import java.util.Arrays;

public class Phone {

    int number;
    String model;
    int weight;

    //конструкторы

    public Phone () {
        System.out.println("Отработал конструктор без параметров");
    }

    public Phone (int number, String model) {
        this.number = number;
        this.model = model;
    }

    public Phone (int number, String model, int weight) {
        this(number, model);  //Вызываю в конструкторе с 3 мя пар-ми конструктор с 2мя
        this.weight = weight;
    }


    //Методы
    public void receiveCall (String nameOfPersonCalling) {
        System.out.println("Входящий звонок ... " + nameOfPersonCalling);
    }

    public void getNumber (int phoneNumber ) {
        System.out.println("Номер телефона " +phoneNumber);
    }

    //Overloading
    public void receivaCall (String nameOfPersonCalling, int phoneNumber) {
        System.out.println("Входящий звонок ... " +nameOfPersonCalling + ". Номер телефона " + phoneNumber);
    }

    static void sendMessage (String [] phoneNumbers) {
        System.out.println(Arrays.toString(phoneNumbers));
    }




}
