package org.levelup.lesson4.homework;

public class Book {

    String nameOfBook;
    String nameOfAuthor;

    public Book (String nameOfBook, String nameOfAuthor) {
        this.nameOfBook = nameOfBook;
        this.nameOfAuthor = nameOfAuthor;
    }

}
