package org.levelup.lesson4.homework;

import static org.levelup.lesson4.homework.Phone.sendMessage;
import static org.levelup.lesson4.homework.Reader.showReaders;
//import static org.levelup.lesson4.homework.Reader.showReaders;

public class Application {

    public static void main(String[] args) {
        System.out.println("------------------------Task1-----------------------------");

        Phone phone1 = new Phone(345, "X100", 450);
        Phone phone2 = new Phone(777,"X200", 600 );
        Phone phone3 = new Phone (908, "X300", 890);
        System.out.println("Модель телефона - " + phone1.model + ". Серийный номер - " +phone1.number + ". Вес модели - " + phone1.weight + "грамм");
        System.out.println("Модель телефона - " + phone2.model + ". Серийный номер - " +phone2.number + ". Вес модели - " + phone2.weight + "грамм");
        System.out.println("Модель телефона - " + phone3.model + ". Серийный номер - " +phone3.number + ". Вес модели - " + phone3.weight + "грамм");

        phone1.receiveCall("Петр Иваныч");
        phone1.getNumber(5023490);

        phone2.receiveCall("Маша");
        phone2.getNumber(634556);

        phone3.receiveCall("Агент Купер");
        phone3.getNumber(911);

        phone1.receivaCall("Мама", 89888780);
        phone2.receivaCall("Папа", 722072);
        phone3.receivaCall("Лучший друг", 234567);

        String [] phoneNumbers = (new String [] {"Отправляю сообщение абонентам: ", "722072", "89888780"});
        sendMessage(phoneNumbers);

        System.out.println("------------------------Task2-----------------------------");

        Reader reader1 = new Reader("Поттер Гарри Иванович", 123, "Гриффиндор", "31.07.1980", 789888);
        Reader reader2 = new Reader("Грейнджер Гермиона Георгиевна", 1, "Гриффиндор", "01.09.1980", 78342);
        Reader reader3 = new Reader("Малфой Драко Дмитриевич", 198, "Слизерин", "06.11.1980", 128790);
        Reader reader4 = new Reader("Дамблдор Альбус Алексеевич", 343, "Хогвартс общий", "23.06.1880", 111);
        Reader [] readers = {reader1, reader2, reader3, reader4};


        Book book1 = new Book("Зельеварение", "С.Снегг");
        Book book2 = new Book("Трансфигурация от ложки до кошки", "М.МакГонагалл");
        Book book3 = new Book("Поймай снитч", "Л.Берциус");
        Book [] allBooks =  new Book[] {book1, book2, book3};



        showReaders(readers);
        System.out.println();
        System.out.println();

        //takeBook
        reader1.takeBook(1);
        reader2.takeBook(17);
        reader3.takeBook(4);
        reader4.takeBook(1);

        String [] booksByReader = (new String[] {"Зельеварение, Снегг", "Транфигурация, МакГонагал"});
        String [] booksByReader2 = (new String[] {"Магические растения, К.Стебль", "Опасности трансгрессии, Д.Джевель"});
        reader3.takeBook(booksByReader);
        reader4.takeBook(booksByReader2);
        System.out.println();


        reader2.takeBook(allBooks);
        reader4.takeBook(allBooks);

        //returnBook
        reader1.returnBook(1);
        reader2.returnBook(15);
        System.out.println();

        reader3.returnBook(booksByReader);
        reader4.returnBook(booksByReader2);
        System.out.println();

        reader1.returnBook(allBooks);
        reader2.returnBook(allBooks);
        System.out.println();




        System.out.println("------------------------Task3-----------------------------");

        Animal dog = new Dog("Пират", "Курица", "д.Орехово, ул.Мира, д.1", true);
        Animal cat = new Cat("Гарфилд", "Рыба", "г. Москва, ул.Ленина, д.1, кв.25", "домашний кот");
        Animal horse = new Horse("Стрела", "Овес", "ул.Ипподромная, д.27", 75);
        Vet vet = new Vet();


        dog.makeNoise();
        cat.eat();
        horse.sleep();

        Animal [] animals = new Animal[] {cat, dog, horse};
        //Отправлять животных на прием к ветеринару
            for (Animal var : animals) {
                System.out.println("На приеме у ветеринара - " + var.name + ", " + var.location);
            }
















    }
}
