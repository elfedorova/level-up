package org.levelup.lesson4.homework;

public class Animal {

    String name;
    String food;
    String location;

    public Animal () {

    }

    public Animal (String name, String food, String location) {
        this.name = name;
        this.food = food;
        this.location = location;
    }

    public void makeNoise() {
        System.out.println("Животное издает звук");
    }

    public void eat () {
        System.out.println("Животное ест");
    }

    public void sleep () {
        System.out.println("Животное спит");
    }






}
