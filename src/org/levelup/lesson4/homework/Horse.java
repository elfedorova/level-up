package org.levelup.lesson4.homework;

public class Horse extends Animal {
    int maxSpeed;

    public Horse () {

    }

    public Horse (String name, String food, String location) {
        super(name, food, location);
    }

    public Horse (String name, String food, String location, int maxSpeed) {
        super(name, food, location);
        this.maxSpeed = maxSpeed;
    }


    @Override
    public void makeNoise() {
        System.out.println("Иго-го");
    }

    @Override
    public void eat () {
        System.out.println("Лошадь ест");
    }

    @Override
    public void sleep () {
        System.out.println("Лошадь спит zzz");

    }



}
