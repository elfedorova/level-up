package org.levelup.lesson4.homework;

public class Cat extends Animal {

    String typeOfCat; //уличная или домашняя

    public Cat () {

    }

    public Cat (String name, String food, String location) {
        super(name, food, location);
    }

    public Cat (String name, String food, String location, String typeOfCat) {
        super(name, food, location);
        this.typeOfCat = typeOfCat;
    }

    @Override
    public void makeNoise() {
        System.out.println("Мяу-мяу");
    }

    @Override
    public void eat () {
        System.out.println("Кошка ест");

    }

    @Override
    public void sleep () {
        System.out.println("Кошка спит zzz");

    }






}
