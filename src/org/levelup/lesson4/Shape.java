package org.levelup.lesson4;

public class Shape { //superclass or basedclass

     int [] sides; //значения длин сторон фигуры

//    Shape () {
//        System.out.println("Execute Shape constructor");
//    }

    Shape (int[] sides) {
        this.sides = sides;
    }

    double perimeter () {
        System.out.println("ShapePerimeter()");
        double sum = 0;
        for (int i = 0; i <sides.length; i++) {
            sum = sum + sides[i];
        }
        return sum;
    }



}
