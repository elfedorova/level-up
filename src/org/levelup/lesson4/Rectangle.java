package org.levelup.lesson4;

public class Rectangle extends Shape { //subclass

//    int [] sides;

    Rectangle () {
        super (new int[0]); //создали пустой массив и передали в констурктор класса Shape,
        // т.к. там конструктор по дефолту отсутствует, но есть другой
        System.out.println("Execute Rectangle constructor");
    }

    Rectangle (int length, int width) {
        super (new int [] {length, width, length, width});
//        this.sides = new int [] {length, width, length, width};
//        super.sides = new int [] {length, width, length, width};
    }

    @Override
    double perimeter() {
        System.out.println("Rectangle Perimeter ()");
        return sides[0] * 2 + sides[1] * 2;
    }

    double square () {
        return  0;
    }

    //    double perimeter () {
//        double sum = 0;
//        for (int i = 0; i <sides.length; i++) {
//            sum = sum + sides[i];
//        }
//        return sum;
//    }

}
