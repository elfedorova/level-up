package org.levelup.lesson4;

public class Application {

    public static void main(String[] args) {
//        Shape s = new Shape();
//        Rectangle r = new Rectangle();
//        System.out.println();

        //shape s = new Shape (4,5,5,4);
        //s.perimeter();

        //Rectangle r = new Rectangle (...);
        //r.perimeter ();   так будет работать, но так громоздко

        int[] shapeSides = {3, 5, 7, 2};

        Shape firstShape = new Shape(shapeSides);  // чтобы исп-ть указанные уже значения
        double firstShapePerimeter = firstShape.perimeter();
        System.out.println("Периметр первой фигуры " + firstShapePerimeter);

        Rectangle firstRectangle = new Rectangle(5, 4);
        double firstRectanglePerimeter = firstRectangle.perimeter();
        System.out.println("Периметр первого прямоугольника " + firstRectanglePerimeter);

        ShapeStorage storage = new ShapeStorage();
        storage.addShape(firstShape);
        storage.addShape(firstRectangle); //firstRectangle --> Shape -> shapes [index]
        storage.addShape(new Rectangle(3,8));
        storage.addShape(new Triangle(4,5,6));

        // shape[0] = Shape
        // shape[1] = Rectangle
        // shape[2] = Rectangle
        // shape[3] = Triangle

        storage.printPerimeters();

        //






    }

}
